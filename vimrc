set cmdheight=2
set number
"set t_Co=256
colorscheme default
set bg=dark
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000

"setl tw=128

command S2 set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
command S4 set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
command S8 set tabstop=8 softtabstop=0 expandtab shiftwidth=8 smarttab
command T4 set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4 smarttab
command T8 set tabstop=8 softtabstop=0 noexpandtab shiftwidth=8 smarttab

S4

filetype indent plugin on
syntax on
set hlsearch
set showcmd
set wildmenu
set ignorecase
set smartcase
set cindent
set ruler
set confirm
set hidden
set laststatus=2
set cmdheight=2
set pastetoggle=<F11>
map <C-h> <Esc>:tabprev<CR>
map <C-l> <Esc>:tabnext<CR>
set tabpagemax=30
set cursorline
hi cursorline		cterm=none
hi CursorLineNr		cterm=none ctermbg=darkblue
hi ColorColumn		ctermbg=lightblue ctermfg=black
"call matchadd('ColorColumn', '\%81v', 100)
set list
set listchars=tab:\ \ ,trail:·,nbsp:~
nnoremap ; :

"buffer switch
nnoremap <tab> :bnext<CR>
nnoremap <S-tab> :bprevious<CR>
